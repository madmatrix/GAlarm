## GAlarm
* 采用了Holo Dark风格简单实现的闹钟小程序；
* 实现定时、重复定时、到期解锁并点亮屏幕的功能；
* 可设置闹钟铃声，支持震动；
* 在MIUI上不能很好的工作；
* 对应博文：http://my.oschina.net/madmatrix/blog/283175
   
---
*[Madmatrix的博客](http://my.oschina.net/madmatrix/blog)*