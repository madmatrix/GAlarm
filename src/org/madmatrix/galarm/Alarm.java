package org.madmatrix.galarm;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import android.media.RingtoneManager;

import com.google.gson.Gson;

public class Alarm {

	public int id;

	/**
	 * 触发时间：小时
	 */
	public int triggerTimeHour;

	/**
	 * 触发时间：分
	 */
	public int triggerTimeMinute;

	/**
	 * 记录用户设置的重复日
	 */
	public List<Weekday> weekdays = new ArrayList<Weekday>();

	/**
	 * 存储未触发的时间的队列
	 */
	public Queue<Long> triggerTimeQueue = new LinkedList<Long>();

	/**
	 * 是否启用
	 */
	public boolean isEnabled;

	/**
	 * 是否重复
	 */
	public boolean isRepeat;

	/**
	 * 是否在响铃的同时震动
	 */
	public boolean isVibrate;

	/**
	 * 闹铃响起时在屏幕上展示的文本
	 */
	public String memo;
	
	/**
	 * 闹钟铃声名称
	 */
	public String alarmRingTitle = "默认";
	
	/**
	 * 闹钟铃声uri
	 */
	public String alarmRingUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM).toString();
	
	public Alarm() {

	}

	public Alarm(int id) {
		this.id = id;
	}
	
	public String toJson() {
		return new Gson().toJson(this);
	}
	
	@Override
	public String toString() {
		return toJson();
	}
}
