package org.madmatrix.galarm;

import java.util.List;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * 监听开机，重启所有活动状态闹钟
 * 
 * @author madmatrix
 */
public class BootBroadcastReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {

		List<Alarm> activeAlarmList = GAlarmManager.loadAllActiveAlarms(context);
		for (Alarm alarm : activeAlarmList) {
			if (!alarm.isEnabled) {
				continue;
			}

			// 重置当前任务
			GAlarmManager.resetAlarm(context, alarm, false);
		}
	}
}
