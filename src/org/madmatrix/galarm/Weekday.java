package org.madmatrix.galarm;

public enum Weekday {

	MONDAY(2), TUESDAY(3), WEDNESDAY(4), THURSDAY(5), FRIDAY(6), SATURDAY(7), SUNDAY(1);
	
	private int value;
	
	Weekday(int value) {
		this.value = value;
	}
	
	public int getValue() {
		return value;
	}
	
	/**
	 * 将星期转为中文字符串数组
	 * 
	 * @return
	 */
	public static String[] toDescArray() {
		return new String[] {"周一", "周二", "周三", "周四", "周五", "周六", "周日"};
	}
}
