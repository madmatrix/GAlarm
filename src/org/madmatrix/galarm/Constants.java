package org.madmatrix.galarm;

public class Constants {

	public static final String CONF_KEY_ALARM = "alarm_conf";
	public static final String CONF_KEY_ALARM_GEN_ID = "alarm_gen_id";
	
	public static final String APP_NAME = "GAlarm";
	
	/**
	 * 闹钟到期后发送此广播
	 */
	public static final String ACTION_ALARM_EXPIRES = "org.madmatrix.galarm.AlarmExpireActivity";
	
	/**
	 * 闹铃类型：闹铃
	 */
	public static final int ALARM_TYPE_ALARM = 1;
	
	/**
	 * 闹铃类型：提醒
	 */
	public static final int ALARM_TYPE_REMINDER = 2;
	
	/**
	 * 返回码：成功
	 */
	public static final int RETCODE_SUCCESS = 0;
	
	/**
	 * 返回码：失败
	 */
	public static final int RETCODE_FAIL = -1;
}
